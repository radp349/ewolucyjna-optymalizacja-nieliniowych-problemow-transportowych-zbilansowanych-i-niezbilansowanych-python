import matplotlib.pyplot as plt
import numpy as np
import random
from pprint import pprint
import math
###################################################################################################################################################
######## Algorytm Ewolucyjny służący do rozwiązywania liniowych i nieliniowych problemów transportowych, zbalnsowanych i niezbalansowanych ########
########################## Stworzony w ramach pracy inżynierskiej "Ewolucyjna Optymalizacja Zadań Transportowych" #################################
########################################################### Autor: Radosław Pawelec ###############################################################
###################################################################################################################################################
np.set_printoptions(suppress=True)
dest =    [50,150,150]
sour = [150,
        250,
        50,
        100]
cost = np.array([[15,6,16],
                 [12,9,17],
                 [30,27,19],
                 [14,18,11]])


# dest =    [9,50,31.5,70,5]
# sour = [30,
#         10.5,
#         15,
#         20.5,
#         8,
#         40]
# cost = np.array([[8 ,13,5 ,9 ,10],
#                  [14,6 ,21,22,13],
#                  [9 ,21,8 ,13,16],
#                  [2 ,3 ,8 ,9 ,19],
#                  [23,9 ,25,14,11],
#                  [20,19,23,21,16]])


# dest =    [9,5,3,7,6,2,8]
# sour = [6,
#         2,
#         4,
#         8,
#         1,
#         3,
#         4,
#         7,
#         5]
# cost = np.array([[8,7,16,11,10,12,5],
#                  [13,16,12,23,10,25,14],
#                  [14,22,17,4,24,13,11],
#                  [16,18,19,25,15,12,7],
#                  [10,20,17,6,11,16,14],
#                  [16,11,6,19,4,14,8],
#                  [25,17,9,12,3,4,16],
#                  [13,8,17,15,20,18,10],
#                  [19,20,25,6,9,4,22]])

###################################################### Paramerty do ustawienia #############################################    
 
# 0 -  pełna reprodukcja (µ,λ), 1 - częściowa reprodukcja (µ+λ)
strategia = 1

# Jeśli stosujemy strategię z pełną reprodukcją (µ,λ) to musi zachodzić równość rozmiar_populacji = rozmiar_potomstwa
# Natomiast jeśli stosujemy strategię z częściową reprodukcją (µ+λ) to musi zachodzić nierówność rozmiar_populacji =< rozmiar_potomstwa

rozmiar_populacji = 50
rozmiar_potomstwa = 70

# numer funkcji kosztu
# 0= schodki, 1 = rampa, 2 = kwadrat, 3 = pierwiastek, 4 = szczyt, 5 = fala, 6 = liniowa
numer_funkcji= 1

# 0 - Rankingowa, 1 - Turniejowa, 2 - Czasem życia
numer_selekcji= 1

# Parametr z przedziału (0.5,1>, jeśli = 1 to turniej deterministyczny, jeśli <1 to losowy
p_turniejowe = 0.9

# Funkcja czasu życia: liniowa - 0, Funkcja proporcjonalne - 1, Funkcja biliniowa - 2
f_czasu = 2

# Szansa reprodukcji każdego osobnika w populacji, używana gdy aktywna jest selekcja sterowana czasem życia
p_reprodukcji = 0.04

# Maksymalny i minimalny czas życia
lmin = 10
lmax = 35

# Szansa wyboru wiersza/kolumny do udziału w operacji mutacji
p_mutacji = 0.75

# p1 - Szansa zajścia mutacji zapewniającej całkowite wartości genów 
# p2 - Szansa zajścia mutacji zapewniającej nieujemne rzeczywiste wartości genów
p1_mutacji = 0.5
p2_mutacji = 1 - p1_mutacji

# Ile co najwyżej miejsc po przecinku mogą mieć wartości genów
ile_po_przecinku = 4


# Współczynniki krzyżowania, dla niecałkowitych wartości genów
# X = c1*U + c2*V  oraz Y = c1*V + c2*U     Rodzice (Macierze U i V), Potomkowie (Macierze X i Y)
c1_krzyzowania = 0.7
c2_krzyzowania = 1 - c1_krzyzowania

liczba_epok = 150

########################################################################################################################


Rodzice = []
Populacja = []
Potomstwo = []
Ocena = []
Ocena_potomstwa = []
Wiek_populacji = []
Wiek_potomstwa = []
Wiek_rodzicow = []


Funkcja = ["schodki", "rampa", "kwadrat", "pierwiastek", "szczyt", "fala","liniowa"]
Balans = ["zbalansowany","niezbalansowany"]
Difficulty = ["Łatwy", "Średni", "Trudny"]


def Balansowanie(cost,balans):
    if sum(sour) > sum(dest):
        dest.append(sum(sour) - sum(dest))
        new_column = np.zeros(len(sour))
        cost = np.insert(cost,len(cost[0]),new_column,axis=1)
        balans = 1
        return cost, balans
        
    elif sum(dest) > sum(sour):
        sour.append(sum(dest) - sum(sour))
        new_row = np.zeros(len(dest))
        cost = np.append(cost,[new_row],axis=0)
        balans = 1
        return cost, balans
    return cost, balans

def Inicjalizacja():
    for n in range(rozmiar_populacji):
        destination = dest.copy()
        source = sour.copy()
        wymiar = len(source)*len(destination)
        osobnik = np.zeros((len(source),len(destination)))
        nieodwiedzone = random.sample(range(wymiar), wymiar)
        
        for q in nieodwiedzone:
            i = int(q/(len(destination)))
            j = q%len(destination)
            
            val = min(source[i], destination[j])
            osobnik[i][j] = val
            
            source[i]-= val
            destination[j]-=val
        Populacja.append(osobnik)



def schodki(ilosc,koszt,S):
    if ilosc>0 and ilosc <= S:
        return 0
    elif ilosc>S and ilosc <= 2*S:
        return koszt    
    elif ilosc>2*S and ilosc <= 3*S:
        return 2*koszt   
    elif ilosc>3*S and ilosc <= 4*S:
        return 3*koszt   
    elif ilosc>4*S and ilosc <= 5*S:
        return 4*koszt  
    elif ilosc>5*S:
        return 5*koszt
    else:
        return 0
    
def rampa(ilosc,koszt,S):
    if ilosc>=0 and ilosc <= S:
        return koszt*(ilosc/S)
    elif ilosc>S and ilosc <= 2*S:
        return koszt
    elif ilosc>2*S:
        return koszt*(1 + (ilosc - 2*S)/S)
    else:
        return 0
        
         
def kwadrat(ilosc,koszt):
    return koszt*ilosc**2

def pierwiastek(ilosc,koszt):
    return koszt*math.sqrt(abs(ilosc))

def szczyt(ilosc,koszt,S):
    return koszt * ( (1 /(1+ (ilosc-2*S)**2))  +  (1/(1+(ilosc-(9/4)*S)**2)) +  (1/(1+(ilosc - (7/4)*S)**2)) ) * S

def fala(ilosc,koszt,S):
    return koszt*ilosc*( math.sin(ilosc*(5*math.pi)/(4*S)) + 1 )

def liniowa(ilosc,koszt):
    return koszt*ilosc



def funkcja(ilosc,koszt,numer_funkcji):
    if numer_funkcji == 0:
        S = int(sum(sour)/(len(sour)*len(dest)))
        return schodki(ilosc,koszt,S)
    elif numer_funkcji == 1:
        S = int(2*sum(sour)/(len(sour)*len(dest)))
        return rampa(ilosc,koszt,S)
        
    elif numer_funkcji == 2:
        return kwadrat(ilosc,koszt)
    
    elif numer_funkcji == 3:
        return pierwiastek(ilosc,koszt)
    
    elif numer_funkcji == 4:
        S = int(2*sum(sour)/(len(sour)*len(dest)))
        return szczyt(ilosc,koszt,S)
    
    elif numer_funkcji == 5:
        S = int(sum(sour)+sum(dest)/(len(sour)+len(dest)))*2
        return fala(ilosc,koszt,S)
    elif numer_funkcji == 6:
        return liniowa(ilosc,koszt)
    
def Przystosowanie(cost,numer_funkcji,ile_po_przecinku):
    Ocena.clear()
    
    for osobnik in Populacja:
        ocena=0
        for i in range(len(osobnik)):
            for j in range(len(osobnik[i])): 
                ocena += funkcja(osobnik[i][j],cost[i][j],numer_funkcji)
        Ocena.append(int(ocena))
        
def Przystosowanie_potomstwa(cost,numer_funkcji,ile_po_przecinku):
    Ocena_potomstwa.clear()
    
    for potomek in Potomstwo:
        ocena=0
        for i in range(len(potomek)):
            for j in range(len(potomek[i])): 
                ocena += funkcja(potomek[i][j],cost[i][j],numer_funkcji)
        Ocena_potomstwa.append(int(ocena))

def Sortuj_Populacje():
    populacja = np.array(Populacja)
    ocena = np.array(Ocena)
    indeks = ocena.argsort()
    populacja = populacja[indeks]
    ocena = ocena[indeks]
    
    Populacja.clear()
    for osobnik in populacja:
        Populacja.append(osobnik)
        
    Ocena.clear()
    for liczba in ocena:
        Ocena.append(liczba)
    
def Rankingowa(rozmiar_populacji,rozmiar_potomstwa):
    suma = sum(range(0,rozmiar_populacji+1))
    wagi = list(range(rozmiar_populacji)+np.ones(rozmiar_populacji))
    wagi.sort(reverse=True)
    
    Sortuj_Populacje()
    Szansa=[]
    
    for i in wagi:
        szansa = (i/suma)*100
        if i != wagi[0]:
            szansa+=Szansa[len(Szansa)-1]
        Szansa.append(szansa)
        
    Los = []
    for j in range(rozmiar_potomstwa):
        los = random.uniform(0, 100)
        Los.append(los)
        
    for los in Los:
        indeks = 0
        while(los>Szansa[indeks]):
            indeks+=1
    
        Potomstwo.append(Populacja[indeks])
        
        
def Turniejowa(rozmiar_populacji,rozmiar_potomstwa,p_turniejowe):
    for i in range(rozmiar_potomstwa):
        
        while(i>=rozmiar_populacji):
            i-=rozmiar_populacji
        
        Turniej = []
        Noty =[]
        
        if i == 0:
            Turniej.append(Populacja[len(Populacja)-1])
            Turniej.append(Populacja[0])
            Noty.append(Ocena[len(Ocena)-1])
            Noty.append(Ocena[0])
            
        else:
            Turniej.append(Populacja[i-1])
            Turniej.append(Populacja[i])
            Noty.append(Ocena[i-1])
            Noty.append(Ocena[i])
            
        szranki = random.uniform(0, 1)
        if p_turniejowe >= szranki:
            Potomstwo.append(Turniej[Noty.index(min(Noty))])
        else:
            Potomstwo.append(Turniej[Noty.index(max(Noty))])


def Nadaj_wiek_populacji(f_czasu,lmin,lmax):
    Wiek_populacji.clear()
    lav = (lmin+lmax)/2
    for i in range(len(Populacja)):
        
        #liniowa
        if f_czasu == 0:
            x = 0
            if max(Ocena) - min(Ocena) == 0:
                x = 1
            czas = lmin + ((lmax-lmin)/(max(Ocena) + x - min(Ocena) ))*(max(Ocena)-Ocena[i])

         
        #proporcjonalna       
        elif f_czasu == 1:
            x = 0
            if max(Ocena) - (sum(Ocena)/len(Ocena)) == 0:
                x = 1
            czas = min(lmax, (lmin + ((lav - lmin)/( max(Ocena) +x - (sum(Ocena)/len(Ocena)))*(max(Ocena)-Ocena[i])  ) ))
        
        #biliniowa
        elif f_czasu == 2: 
            x = 0
            if max(Ocena) - (sum(Ocena)/len(Ocena)) == 0:
                x = 1
            elif sum(Ocena)/len(Ocena) - min(Ocena) == 0:
                x = 1
            if(Ocena[i] > sum(Ocena)/len(Ocena)):
                czas = lmin + ((lav-lmin)/ ((max(Ocena) +x - (sum(Ocena)/len(Ocena))) )) *(max(Ocena)-Ocena[i])
            
            elif(Ocena[i] <= sum(Ocena)/len(Ocena)):
                czas = lav + ((lmax-lav)/ ((sum(Ocena)/len(Ocena) +x - min(Ocena)) )) *(max(Ocena)-Ocena[i])
            
            
        if czas > lmax:
            czas = lmax
        elif czas < lmin:
            czas= lmin

        Wiek_populacji.append(round(czas))



def Nadaj_wiek_potomstwu(numer_selekcji,f_czasu,lmin,lmax):
    if numer_selekcji == 2:
        if len(Potomstwo)!= 0:
            Wiek_potomstwa.clear()
            lav = (lmin+lmax)/2
            for i in range(len(Potomstwo)):
                
                #liniowa
                if f_czasu == 0:
                    x = 0
                    if (min(Ocena_potomstwa)) == max(Ocena_potomstwa):
                        x = 1
                    czas = lmin + ((lmax-lmin)/(max(Ocena_potomstwa) + x - min(Ocena_potomstwa)))*(max(Ocena_potomstwa)-Ocena_potomstwa[i])

                
                #proporcjonalna       
                elif f_czasu == 1:
                    x = 0
                    if max(Ocena_potomstwa) - (sum(Ocena_potomstwa)/len(Ocena_potomstwa)) == 0:
                        x = 1
                    czas = min(lmax, (lmin + ((lav - lmin)/( max(Ocena_potomstwa) + x - (sum(Ocena_potomstwa)/len(Ocena_potomstwa)))*(max(Ocena_potomstwa)-Ocena_potomstwa[i])  ) ))
                
                #biliniowa
                elif f_czasu == 2:
                    x = 0
                        
                    if(Ocena[i] > sum(Ocena)/len(Ocena)):
                        if max(Ocena_potomstwa) - (sum(Ocena_potomstwa)/len(Ocena_potomstwa)) == 0:
                            x = 1
                        czas = lmin + ((lav-lmin)/ ((max(Ocena_potomstwa) + x -  (sum(Ocena_potomstwa)/len(Ocena_potomstwa))) )) *(max(Ocena_potomstwa)-Ocena_potomstwa[i])
                    
                    elif(Ocena[i] <= sum(Ocena)/len(Ocena)):
                        if ((sum(Ocena_potomstwa)/len(Ocena_potomstwa)) - min(Ocena_potomstwa)) == 0:
                            x = 1
                        czas = lav + ((lmax-lav)/ (    ((sum(Ocena_potomstwa)/len(Ocena_potomstwa)) + x - min(Ocena_potomstwa))  )  ) *(max(Ocena_potomstwa)-Ocena_potomstwa[i])
                    
                    
                if czas > lmax:
                    czas = lmax
                elif czas < lmin:
                    czas= lmin

                Wiek_potomstwa.append(round(czas))
        

                
def Selekcja(rozmiar_populacji,rozmiar_potomstwa,numer_selekcji,p_turniejowe,p_reprodukcji):
    global Potomstwo
    Potomstwo.clear()

    #rankingowa z kolem ruletki
    if numer_selekcji == 0:
        Rankingowa(rozmiar_populacji,rozmiar_potomstwa)
    #turniejowa
    elif numer_selekcji == 1:
        Turniejowa(rozmiar_populacji,rozmiar_potomstwa,p_turniejowe)
    elif numer_selekcji ==2:
        if (len(Populacja)) < 500:
            for osobnik in Populacja:
                los = random.uniform(0, 1)
                if los < p_reprodukcji:
                    Potomstwo.append(osobnik)
            

 
def Mutacja1(osobnik,temp_sour,temp_dest):
    temp_sour_val=[]
    temp_dest_val=[]
    
    array = np.zeros((len(temp_sour),len(temp_dest)))
    for i in range(array.shape[0]):
        for j in range(array.shape[1]):
            array[i][j] = osobnik[temp_sour[i]][temp_dest[j]]
    
    for row in array:
        temp_sour_val.append(round(sum(row),ile_po_przecinku))

    for col in zip(*array):
        temp_dest_val.append(round(sum(col),ile_po_przecinku))
        
    wymiar = len(temp_sour)*len(temp_dest)
    array = np.zeros((len(temp_sour),len(temp_dest)))
    nieodwiedzone = random.sample(range(wymiar), wymiar)
    
    for q in nieodwiedzone:
        i = int(q/(len(temp_dest_val)))
        j = q%len(temp_dest_val)
        
        val = min(temp_sour_val[i], temp_dest_val[j])
        array[i][j] = val
        
        temp_sour_val[i] -= val
        temp_dest_val[j] -= val

    for i in range(array.shape[0]):
        for j in range(array.shape[1]):
            osobnik[temp_sour[i]][temp_dest[j]] = array[i][j]    
    
def Mutacja2(osobnik,temp_sour,temp_dest,ile_po_przecinku):
    temp_sour_val=[]
    temp_dest_val=[]
    
    array = np.zeros((len(temp_sour),len(temp_dest)))
    for i in range(array.shape[0]):
        for j in range(array.shape[1]):
            array[i][j] = osobnik[temp_sour[i]][temp_dest[j]]
    
    for row in array:
        temp_sour_val.append(round(sum(row),ile_po_przecinku))

    for col in zip(*array):
        temp_dest_val.append(round(sum(col),ile_po_przecinku))
        
    wymiar = len(temp_sour)*len(temp_dest)
    array = np.zeros((len(temp_sour),len(temp_dest)))
    nieodwiedzone = random.sample(range(wymiar), wymiar)
    ostatni = np.ones((len(temp_sour),len(temp_dest)))
    ostatniT = np.transpose(ostatni)

    for q in nieodwiedzone:
        i = int(q/(len(temp_dest_val)))
        j = q%len(temp_dest_val)
        ostatni[i][j]=0
        ostatniT[j][i]=0
        
        val1 = min(temp_sour_val[i], temp_dest_val[j])
        if ((sum(ostatni[i])==0) or sum(ostatniT[j])==0):
            val = val1
        else:
            val = random.randint(0,int ( val1*(10**( ile_po_przecinku ) ) ) )   /   (10**(ile_po_przecinku))        
            if((val<10**(-1*(ile_po_przecinku)))and val>0):
                val=0  
        array[i][j] = val
        temp_sour_val[i] -= val
        temp_dest_val[j] -= val

    for i in range(array.shape[0]):
        for j in range(array.shape[1]): 
            array[i][j] += min(temp_sour_val[i],temp_dest_val[j])
            temp_sour_val[i] -= min(temp_sour_val[i],temp_dest_val[j])
            temp_dest_val[j] -= min(temp_sour_val[i],temp_dest_val[j])
            osobnik[temp_sour[i]][temp_dest[j]] = array[i][j]
 
def Mutacja(p_mutacji,p1_mutacji,p2_mutacji,ile_po_przecinku):
    
    for osobnik in Potomstwo:
        temp_sour = []
        temp_dest = []
        
        for i in range(len(sour)):
            los = random.uniform(0, 1)
            if(los<=p_mutacji):
                temp_sour.append(i)
                
        for j in range(len(dest)):
            los = random.uniform(0, 1)
            if(los<=p_mutacji):
                temp_dest.append(j)
                
        if ((len(temp_dest)>1) and (len(temp_sour)>1)):
            
            los = random.uniform(0, 1)
            if (p1_mutacji != 0) and (los <= p1_mutacji):
                Mutacja1(osobnik,temp_sour,temp_dest)
            elif (los <= (p1_mutacji+p2_mutacji)):
                Mutacja2(osobnik,temp_sour,temp_dest,ile_po_przecinku)

def Krzyzowanie1(U,V):
    div = (U + V)/2
    rem = (U + V)%2
    rem1 = np.zeros((rem.shape[0],rem.shape[1]))
    rem2 = np.zeros((rem.shape[0],rem.shape[1]))

    alternate = False            
    for i in range(div.shape[0]):
        for j in range(div.shape[1]):
            div[i][j] = int(div[i][j])
            
            if rem[i][j] == 1:
                if alternate:
                    rem1[i][j] = 1
                else:
                    rem2[i][j] = 1
                alternate = not alternate
    rem1T = np.transpose(rem1)
    rem2T = np.transpose(rem2)
    for col in range (rem.shape[1]):
        if sum(rem1T[col]) != sum(rem2T[col]):
            X = U
            Y = V
            return X,Y
    X = (div + rem1)
    Y = (div + rem2) 
            
    return X,Y

def Zaokraglenie(AD,CF,ile_po_przecinku):
    AD /= (10**(ile_po_przecinku))
    CF -= AD
    temp_sour_val = []
    temp_dest_val = []
    for row in AD:
        temp_sour_val.append(round(sum(row),ile_po_przecinku))
    for col in zip(*AD):
        temp_dest_val.append(round(sum(col),ile_po_przecinku))
        
    wymiar = CF.shape[0]*CF.shape[1]
    nieodwiedzone = random.sample(range(wymiar), wymiar)
    
    for q in nieodwiedzone:
        i = int(q/(CF.shape[1]))
        j = q % CF.shape[1]
        val = min(temp_sour_val[i], temp_dest_val[j])
        CF[i][j] += val
        temp_sour_val[i]-= val
        temp_dest_val[j]-= val
        
    return CF

def Krzyzowanie2(U,V,c1_krzyzowania,c2_krzyzowania,ile_po_przecinku):
    
    X = c1_krzyzowania*U + c2_krzyzowania*V
    Y = c1_krzyzowania*V + c2_krzyzowania*U
    
    A = X * (10**(ile_po_przecinku))
    B = X * (10**(ile_po_przecinku))
    C = X * 1
    D = Y * (10**(ile_po_przecinku))
    E = Y * (10**(ile_po_przecinku))
    F = Y * 1

    round1 = False
    round2 = False
    for i in range(A.shape[0]):
        for j in range(A.shape[1]):
            B[i][j] = int(B[i][j])
            E[i][j] = int(E[i][j])
            
            A[i][j] -= B[i][j]
            D[i][j] -= E[i][j]
            if A[i][j] != 0:
                round1 = True
            if D[i][j] != 0:
                round2 = True
                
    if round1:
        X = Zaokraglenie(A,C,ile_po_przecinku)
    if round2:
        Y = Zaokraglenie(D,F,ile_po_przecinku)
    
    return X,Y   
    
def Krzyzowanie(c1_krzyzowania,c2_krzyzowania,ile_po_przecinku):
    liczba_krzyzowan = len(Potomstwo)
    if liczba_krzyzowan % 2 == 1:
        liczba_krzyzowan -= 1
    liczba_krzyzowan /= 2
    
    for i in range(int(liczba_krzyzowan)):
        U = Potomstwo[(2*i)]
        V = Potomstwo[(2*i)+1]
        
        if ((ile_po_przecinku == 0) and (len(sour)*(len(dest)) <= 15)):
            X, Y = Krzyzowanie1(U,V)
            Potomstwo[(2*i)] = X
            Potomstwo[(2*i)+1] = Y
        else:
            X, Y = Krzyzowanie2(U,V,c1_krzyzowania,c2_krzyzowania,ile_po_przecinku)
            Potomstwo[(2*i)] = X
            Potomstwo[(2*i)+1] = Y
 
 
def Strategia(strategia,rozmiar_populacji,cost,numer_funkcji,numer_selekcji,f_czasu,lmin,lmax):
    global Wiek_populacji
    Rodzice.clear()
    Wiek_rodzicow.clear()
    
    if numer_selekcji == 2:
        Nadaj_wiek_potomstwu(numer_selekcji,f_czasu,lmin,lmax)

        for osobnik in range(len(Populacja)):
            if Wiek_populacji[osobnik] > 0:
                Rodzice.append(Populacja[osobnik])
                Wiek_rodzicow.append(Wiek_populacji[osobnik])
                
        for potomek in range(len(Potomstwo)):
            Rodzice.append(Potomstwo[potomek])
            Wiek_rodzicow.append(Wiek_potomstwa[potomek])
            
        if(len(Rodzice)) > 2:
            Wiek_rodzicow[:] = [wiek - 1 for wiek in Wiek_rodzicow]

        
        Wiek_populacji.clear()

        Wiek_populacji = Wiek_rodzicow.copy()
            
        
    elif strategia==0:
        for osobnik in Potomstwo:
            Rodzice.append(osobnik)
    elif strategia==1:
        for osobnik in Potomstwo:
            Populacja.append(osobnik)
        Przystosowanie(cost,numer_funkcji,ile_po_przecinku)
        Sortuj_Populacje()
        for i in range(rozmiar_populacji):
            Rodzice.append(Populacja[i])
            
    Potomstwo.clear()
    Populacja.clear()
    
    for osobnik in Rodzice:
        Populacja.append(osobnik)
    Przystosowanie(cost,numer_funkcji,ile_po_przecinku)
    Sortuj_Populacje()    
        
def Najlepsze_rozwiazanie(least,least_val,least_epoch,epoka):
    if Ocena[0]<least_val:
        least = np.copy(Populacja[0])
        least_val = Ocena[0]*1
        least_epoch = epoka
    return least, least_val,least_epoch
    


minimal = []
average = []
xxx = np.linspace(0, liczba_epok-1, num=liczba_epok)

least = np.zeros((len(sour),len(dest)))
least_val = math.inf
least_epoch = 0
balans = 0
difficulty = 0
if len(sour)*len(dest) >= 50:
    difficulty = 2
elif len(sour)*len(dest) >= 19:
    difficulty = 1   


cost, balans = Balansowanie(cost, balans)
Inicjalizacja()
Przystosowanie(cost,numer_funkcji,ile_po_przecinku)
Nadaj_wiek_populacji(f_czasu,lmin,lmax)
for epoka in range(liczba_epok):
    Przystosowanie(cost,numer_funkcji,ile_po_przecinku)
    Selekcja(rozmiar_populacji,rozmiar_potomstwa,numer_selekcji,p_turniejowe,p_reprodukcji)
    Krzyzowanie(c1_krzyzowania,c2_krzyzowania,ile_po_przecinku)
    Mutacja(p_mutacji,p1_mutacji,p2_mutacji,ile_po_przecinku)
    Przystosowanie_potomstwa(cost,numer_funkcji,ile_po_przecinku)
    Nadaj_wiek_potomstwu(numer_selekcji,f_czasu,lmin,lmax)
    Strategia(strategia,rozmiar_populacji,cost,numer_funkcji,numer_selekcji,f_czasu,lmin,lmax)
    least,least_val,least_epoch = Najlepsze_rozwiazanie(least,least_val,least_epoch,epoka)   
    minimal.append(min(Ocena))
    average.append(sum(Ocena)/len(Populacja))

print(f"Najbardziej optymalne rozwiązanie o wartości funkcji kosztu {least_val} znaleziono w epoce nr {least_epoch}, przyjmuje ono postać:")
pprint(least)
    
plt.plot(xxx,minimal, 'b-', label='Minimalna wartość funkcji przystosowania w populacji X')
plt.plot(xxx, average, 'g--', label='Średnia wartość funkcji przystosowania w populacji X')
plt.xlabel('Numer Populacji')
plt.ylabel('Wartość funkcji przystosowania')
plt.title(f"{Difficulty[difficulty]} problem {Balans[balans]} rozmiaru {len(sour)} x {len(dest)} o funkcji kosztu: {Funkcja[numer_funkcji]}")
plt.legend()
plt.show()
    
    

    


   
    



    





        

